import os,cv2,pytesseract
from flask import Flask, render_template, request,jsonify
from flask_cors import CORS
from PIL import Image
import csv
import pandas as pd
import argparse
import datetime
import sqlalchemy
from flask_sqlalchemy import SQLAlchemy




pytesseract.pytesseract.tesseract_cmd = "C:\\Users\\PC-YARA\\AppData\\Local\\Tesseract-OCR\\tesseract"

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://{root}:{1234}@{35.223.240.47}/{etatfinancier}'

db = SQLAlchemy(app)
CORS(app)


UPLOAD_FOLDER = os.path.basename('.')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER



@app.route('/')
def index():
    return render_template('index.html')


@app.route('/api/ocr', methods=['POST','GET'])
def upload_file():
    if request.method == "GET":
        return "This is the api BLah blah"
    elif request.method == "POST":
        file = request.files['image']

        f = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)

        # add your custom code to check that the uploaded file is a valid image and not a malicious file (out-of-scope for this post)
        file.save(f)
        # print(file.filename)

        image = cv2.imread(UPLOAD_FOLDER+"/"+file.filename)
        os.remove(UPLOAD_FOLDER+"/"+file.filename)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # check to see if we should apply thresholding to preprocess the
        # image
        preprocess = request.form["preprocess"]
        if  preprocess == "thresh":
            gray = cv2.threshold(gray, 0, 255,
                                 cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

        # make a check to see if median blurring should be done to remove
        # noise

        elif preprocess == "blur":
            gray = cv2.medianBlur(gray, 3)
        print(preprocess)
        # write the grayscale image to disk as a temporary file so we can
        # apply OCR to it
        filename = "{}.png".format(os.getpid())
        cv2.imwrite(filename, gray)
        # load the image as a PIL/Pillow image, apply OCR, and then delete
        # the temporary file
        # print("C:/Users/mzm/PycharmProjects/My_website/ocr_using_video/"+filename,Image.open("C:\\Users\mzm\PycharmProjects\My_website\ocr_using_video\\"+filename))
        text = pytesseract.image_to_string(Image.open(filename))
        os.remove(filename)
        with open('Test.csv', 'w', encoding='utf-8') as f:
            #json.dump(text, f, indent= 14)
            f.write(text)
data = pd.read_csv('Test.csv', sep=",\s+|;|-|_", delimiter=', - _', encoding="utf-8", skipinitialspace=True, engine='python')
print (data)
data.to_sql(con=app.config, name='finance', if_exists='replace')
    
#app.run("0.0.0.0",5000,threaded=True,debug=True)
if __name__ == '__main__':
    app.run(debug=True)


#data.columns = [col.replace(",", "") for col in data.columns]
#etat_df = pd.DataFrame(columns=column_names)
#for index, row in data.iterrows():
    #print (row[1]['emailid'])

#print(data.iloc[0:5,0:2])



